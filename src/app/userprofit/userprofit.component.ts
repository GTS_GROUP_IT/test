import { IuserService } from './../share/service/iuser.service';
import { Component, OnInit } from '@angular/core';
import { Iuser } from '../share/entities/iuser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-userprofit',
  templateUrl: './userprofit.component.html',
  styleUrls: ['./userprofit.component.scss']
})
export class UserprofitComponent implements OnInit {
  private user: Iuser;

  constructor(private usserService: IuserService,
    private router: Router) { }

  ngOnInit() {
    this.usserService.getUserByToken()
      .subscribe(response => {
        this.user = response;
      });
  }
  updateUser() {
    const request = {
      firstname: this.user.firstname,
      password: this.user.password,
      lastname: this.user.lastname,
      country: this.user.country,
      address: this.user.address,
      slogan: this.user.slogan,
      company: this.user.company,
      city: this.user.city
    };
    this.usserService.updateUser(request)
      .subscribe(response => {
        if (response) {
          console.log(response);
          this.router.navigate(['./home2']);
        }
      });
  }
}
