export class Iuser {
    _id?: string;
    firstname?: string;
    email?: string;
    password?: string;
    img?: string;
    lastname?: string;
    country?: string;
    address?: string;
    slogan?: string;
    company?: string;
    city?: string;
}
